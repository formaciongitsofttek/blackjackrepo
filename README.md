# Java CRUD App
El objetivo no es hacer funcionar la aplicación no obstante se recomienda intentar hacer código lo más autentico posible.

## Ejercicios Propuestos:

- Desde la rama `master` agrega una carpeta que se llame `cracks` y dentro de esa carpeta añade un archivo que sea `tu_nombre.txt` el archivo puede contener una pequeña bio tuya si lo deseas.

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Crea una rama con tu `nombre/modificandobio` que derive de `desarrollo`.
Modifica la bio que antes as logrado añadir.
  
¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Crea una rama que derive de `desarollo` llamada `tunombre/not_conflicts` y modifica el archivo que se te haya asignado, si no se te ha asignado ninguno, coge uno al azar en la carpeta `src/models` o `src/gui`
¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

## Recuerda usar código lo más parecido al real:

Aunque pueda parecer irrelevante git utiliza herramientas para poder hacer auto-merge.
Estás están basadas en analisis de código y no funcionan con prosa normal.

- Crea una rama que derive de `desarollo` llamada `tunombre/not_conflicts` y modifica el archivo que se te haya asignado, si no se te ha asignado ninguno, asegurate de coger alguno que ya estén modificando uno o varios de tus compañeros.

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Si en el anterior ejemplo fuiste el primero en modificar y pushear, no recibiste nigún conflicto, crea otra rama y vuelve a modificar el archivo, esta vez espera que alguien más haya modificado el archivo, si no hay nadie modificandolo yá, comentalo y que alguien añada algo de código de copiar pegar, para recibir tu también el conflicto.

- Crea una rama que derive de `desarollo`  y modifica el archivo que se te haya asignado, si no se te ha asignado ninguno, asegurate de coger alguno que ya estén modificando uno o varios de tus compañeros, En esta ocasión TODOS tenemos que hacer las modificaciones entre la linea 8 y la linea 25 (copiar pegar alguna función de otra parte de código o incluso de otro archivo).

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Crea una rama y deshaz los cambios que hayas hecho en los anteriores ejercicos (exceptuando el primero donde está tu bio.)

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Crea una rama y coge los cambios de 3 commits consecutivos que veas en el historial que cambién el estado actual del repositorio (no vale si se va a quedar todo igual).

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Checkea la "salud" de master respecto a desarrollo.

¿Hace cuanto que no se sincronizan?
¿A que punto nos ha llevado?
¿Se puede mergear desarrollo y master en este momento sin problemas?

- Crea una rama desde desarrollo, modifica todos los archivos que se encuentran dentro de la carpeta `src/models`, copia, pega, corta, vacía... y sube los cambios.

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Borra los archivos de bio de todos tus compañeros, incluido el tuyo, puedes borrar la carpeta también si quieres.

Sincroniza los cambios.

- Deshaz el borrado de los archivos que acabas de borrar.

Sincroniza los cambios.


- Crea una rama, modifica cualquier archivo, de conflicto o no, genera un pull request y asegurate de que la rama se ha borrado en remoto. Ahora una vez ha sido mergeado y la rama remota borrada, CONTINUA EN LA RAMA LOCAL haz cambios y sincronizate con remoto.

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Crea una rama y trabaja un rato en ella, cambia de rama para trabajar en otra cosa "más urgente" , cambia un par de archivos en la nueva rama y pushea, vuelve a la rama anterior y continúa por donde lo habías dejado.

¿Que problemas te has encontrado?
¿Porqué crees que está pasando?
¿Como solucionarlo?

- Crea un hook que no te permita hacer push si estás en master.

El código es el siguiente, colócalo tú y pruebalo:

```
#!/bin/bash
protected_branch='master'
current_branch=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')

if [ $protected_branch = $current_branch ]
    then
        read -p "You're about to push to master. What's the meaning of life? > " -n 2 -r < /dev/tty
        echo
            #if echo $REPLY | grep -E '^[Yy]$' > /dev/null
            if echo $REPLY | grep -E '42' > /dev/null
            then
                exit 0 # carry on
            fi
            exit 1 # exit and go read a book
else
    exit 0
fi
```

## BOLA EXTRA:
- Realiza todos los ejercicios anteriores desde un gui (ide o gui favorito), recomendado `gitkraken`